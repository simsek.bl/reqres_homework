package stepDef;

import com.jayway.jsonpath.JsonPath;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pojoClasses.CreateUser;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static utils.APIUtils.serializePOJO;
import static utils.ConfigReader.getProperty;
@Data
public class ReqresStepDef {

    Response response;
    CreateUser createUser;
    int userID;

    private static Logger logger = LogManager.getLogger(ReqresStepDef.class);

    @Given("Create user with {string}, {string}")
    public void createUserWith(String name, String job) {

        CreateUser createUser = CreateUser
                .builder().name(name).job(job).build();

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(serializePOJO(createUser))
                .post(getProperty("reqresURL") + "/api/users/")
                .then().log().all().extract().response();

        userID = Integer.parseInt(JsonPath.read(response.asString(), "id"));


    }

    @And("Validate that status code is {int}")
    public void validateThatStatusCodeIs(int statusCode) {

        int actualStatusCode = response.getStatusCode();

        logger.debug("Status code: " + response.getStatusCode());
        logger.error("Status code: " + response.getStatusCode());
        assertThat(
                "I am expecting status code: " + statusCode,
                actualStatusCode,
                is(statusCode)
        );
    }

    @And("Make GET call to get user with {string}")
    public void makeGETCallToGetUserWith(String url) {


        logger.debug("The user ID " + userID + " should be created.");
        logger.error("The user ID " + userID + " should be created.");
        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .get(url + userID)
                .then().log().all().extract().response();
    }

    @And("Updating the user with the following data")
    public void updatingTheUserWithTheFollowingData(Map<String, String> data) {

        CreateUser createUser = CreateUser
                .builder().name(data.get("name"))
                .job(data.get("job")).build();

        logger.debug("The user ID " + userID + " should be updated.");
        logger.error("The user ID " + userID + " should be updated.");
        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(serializePOJO(createUser))
                .patch(getProperty("reqresURL") + "/api/users/" + userID)
                .then().log().all().extract().response();

    }

    @When("I delete user")
    public void iDeleteUser() {

        logger.debug("The user ID " + userID + " should be deleted.");
        logger.error("The user ID " + userID + " should be deleted.");
        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .delete(getProperty("reqresURL") + "/api/users/" + userID)
                .then().log().all().assertThat().statusCode(204).extract().response();
    }
}
